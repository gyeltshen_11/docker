# Docker



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gyeltshen_11/docker.git
git branch -M main
git push -uf origin main
```
<h1 align="center">Hi 👋, I'm Gyeltshen wangdi</h1>
<h3 align="center">Docker</h3>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=gyeltshenwang&label=Profile%20views&color=0e75b6&style=flat" alt="gyeltshenwang" /> </p>

<h3 align="left">Connect with me:</h3>
<p align="left">
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://nodejs.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> </a> </p>

<h3 align="left">Support:</h3>
<p><a href="https://www.buymeacoffee.com/gyeltshen_11"> <img align="left" src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="gyeltshen_11" /></a></p><br><br>

<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=gyeltshenwang&show_icons=true&locale=en&layout=compact" alt="gyeltshenwang" /></p>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=gyeltshenwang&show_icons=true&locale=en" alt="gyeltshenwang" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=gyeltshenwang&" alt="gyeltshenwang" /></p>
